'use strict';

module.exports = function(Customer) {
Customer.createAccount = function(id,ownerId,accountBalance,cb){
    Customer.app.models.Account.create({
          id: id,
        accountBalance: accountBalance,
          ownerId: ownerId,
          creationDate: Date.now()
        },function(err, resp){
          cb(err, resp)
        });
Customer.app.models.Email.send({
      to: 'zunnurainbadar@gmail.com',
      from: 'zunnurain@hybridlogics.com',
      subject: 'Welcome to hybridlogics',
      text: 'Thanx for creating an account',
      html: 'Thanx for you time we appreciate that you have created an account here!'
    }, function(err, mail) {
      if(err)
      {
        console.log(err);
      }
      else{
      console.log('email sent!');
      }
    });
}
Customer.remoteMethod('createAccount',{
    accepts:[{arg:'id',type:'string'},
    {arg:'ownerId',type:'string'},
    {arg:'accountBalance',type:'object'},
    ],
    returns:[{arg:'id',type:'string'},
    {arg:'ownerId',type:'string'},
     {arg:'accountBalance',type:'object'},
    ]
});
//  Customer.sendEmail = function(cb) {
//   }
//   Customer.remoteMethod('sendEmail',{
//     accepts:[{arg:'id',type:'string'},
//     {arg:'ownerId',type:'string'},
//     {arg:'accountBalance',type:'object'},
//     ],
//     returns:[{arg:'id',type:'string'},
//     {arg:'ownerId',type:'string'},
//      {arg:'accountBalance',type:'object'},
//     ]
// });
//format of array [{ "currency" : "PKR", "amount" : 500}, { "currency" : "$", "amount" : 10}]
};
