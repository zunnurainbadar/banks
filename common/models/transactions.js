'use strict';

   var app = require("../../server/server");
 module.exports = function(Transactions) {

Transactions.deposit = function(id,amountDep,currency,cb){
    Transactions.app.models.Account.findOne({where:{_id:id}},function(err,document){
            console.log('This is loop '+document.accountBalance.usd)
            console.log('This is given currency ' + currency);
        if(currency =="usd")
        {
            console.log('thisis dollar currency');
        Transactions.app.models.Account.update({_id:id}, { "accountBalance.usd": document.accountBalance.usd + amountDep },function(err,document){            
});
    Transactions.create({
          id: "1",
          description:"Deposit",
          accountId:id,
          transectionType:"credit",
          transectionFrom:"transectionFrom",
          referenceId:"23",
          amount:amountDep,
          currency:currency,
          transactionDate:Date.now()
        },function(err, resp){
            if(err){
                console.log('There is an error '+ err);
            }
            console.log('Transaction of deposit completed');
        });
        }
        else if(currency =="pkr")
        {
            console.log('this is PKR currency');
        Transactions.app.models.Account.update({_id:id}, { "accountBalance.pkr":  document.accountBalance.pkr + amountDep},function(err,document){
    console.log('Record updated successfully');
    cb(null,document);
});
 Transactions.create({
          id: "2",
          description:"Deposit",
          accountId:id,
          transectionType:"credit",
          transectionFrom:"transectionFrom",
          referenceId:"23",
          amount:amountDep,
          currency:currency,
          transactionDate:Date.now()
        },function(err, resp){
        });
    }
    Transactions.app.models.Email.send({
      to: 'zunnurainbadar@gmail.com',
      from: 'zunnurain@hybridlogics.com',
      subject: 'Welcome to hybridlogics',
      text: 'Thanx for creating an account',
      html: 'Thanx for you time! You deposit '+amountDep + ' '+ currency + ' in your account'
    }, function(err, mail) {
      if(err)
      {
        console.log(err);
      }
      else{
      console.log('email sent!');
      }
    });  
    });
}
Transactions.remoteMethod('deposit',{
    accepts:[{arg:'id',type:'string'},
    {arg:'amountDep',type:'number'},
    {arg:'currency',type:'string'}
    ],
    returns:[{arg:'id',type:'string'},
    {arg:'amountDep',type:'number'},
    {arg:'currency',type:'string'}
    ]
});
Transactions.withdraw = function(id,amountWit,currency,cb){
        Transactions.app.models.Account.findOne({where:{_id:id}},function(err,document){
            if(document.accountBalance.pkr <= 0 && document.accountBalance.pkr <=0 )
            {
                console.log("error");
            }
            else{
            console.log('This is loop '+document.accountBalance.usd)
            console.log('This is given currency ' + currency);
        if(currency =="usd")
        {
            console.log('thisis dollar currency');
        Transactions.app.models.Account.update({_id:id}, {"accountBalance.usd":  document.accountBalance.usd - amountWit},function(err,document){
    console.log('Record updated successfully');
    cb(null,document);
});
Transactions.create({
          id: "3",
          description:"Withdraw",
          accountId:id,
          transectionType:"Debit",
          transectionFrom:document.ownerId,
          referenceId:"23",
          amount:amountWit,
          currency:currency,
          transactionDate:Date.now()
        },function(err, resp){
        
    });
        }
        else if(currency =="pkr")
        {
            console.log('this is PKR currency');
        Transactions.app.models.Account.update({_id:id}, { "accountBalance.pkr":  document.accountBalance.pkr - amountWit},function(err,document){
    console.log('Record updated successfully');
    cb(null,document);
});
Transactions.create({
          id: "4",
          description:"Withdraw",
          accountId:id,
          transectionType:"Debit",
          transectionFrom:document.ownerId,
          referenceId:"23",
          amount:amountWit,
          currency:currency,
          transactionDate:Date.now()
        },function(err, resp){
        
    });
        }
        Transactions.app.models.Email.send({
      to: 'zunnurainbadar@gmail.com',
      from: 'zunnurain@hybridlogics.com',
      subject: 'Welcome to hybridlogics',
      text: 'Thanx for creating an account',
      html: 'Thanx for you time!You withdraw '+amountWit + ' '+ currency + ' in your account'
    }, function(err, mail) {
      if(err)
      {
        console.log(err);
      }
      else{
      console.log('email sent!');
      }
    });
}
    });
    
}
Transactions.remoteMethod('withdraw',{
    accepts:[{arg:'id',type:'string'},
    {arg:'amountWit',type:'number'},
    {arg:'currency',type:'string'}
    ],
    returns:[{arg:'id', type:'document'},
    {arg:'amountWit',type:'document'},
    {arg:'currency',type:'string'}
    ]
});
Transactions.transfer = function(Sid,Rid,amount,currency,cb){
    Transactions.app.models.Account.findOne({where:{_id:Sid}},function(err,document){
         if(document.accountBalance.pkr <= 0 && document.accountBalance.pkr <=0 )
            {
                console.log("error");
            }
            else{
                if(currency == "usd")
                {
                    Transactions.app.models.Account.update({_id:Sid}, {"accountBalance.usd":  document.accountBalance.usd - amount},function(err,document){
    console.log('Record updated successfully');
});
Transactions.create({
          id: "5",
          description:"Transfer From",
          accountId:Sid,
          transectionType:"Debit",
          transectionFrom:document.ownerId,
          referenceId:"23",
          amount:amount,
          currency:currency,
          transactionDate:Date.now()
        },function(err, resp){
        });
        Transactions.app.models.Email.send({
      to: 'zunnurainbadar@gmail.com',
      from: 'zunnurain@hybridlogics.com',
      subject: 'Welcome to hybridlogics',
      text: 'Thanx for creating an account',
      html: 'Thanx for you time !Your account has been debited.You have send '+amount +' usd from your aaccount'
    }, function(err, mail) {
      if(err)
      {
        console.log(err);
      }
      else{
      console.log('email sent!');
      }
    }); 
        Transactions.app.models.Account.findOne({where:{_id:Rid}},function(err,document){
        Transactions.app.models.Account.update({_id:Rid}, {"accountBalance.usd":  document.accountBalance.usd + amount},function(err,document){
    console.log('Record updated successfully');
        cb(null,document);
});
Transactions.create({
          id: "6",
          description:"Transfer To",
          accountId:Rid,
          transectionType:"Credit",
          transectionFrom:document.ownerId,
          referenceId:"23",
          amount:amount,
          currency:currency,
          transactionDate:Date.now()
        },function(err, resp){
        });
                Transactions.app.models.Email.send({
      to: 'zunnurainbadar@gmail.com',
      from: 'zunnurain@hybridlogics.com',
      subject: 'Welcome to hybridlogics',
      text: 'Thanx for creating an account',
      html: 'Thanx for you time !Your account has been credited .You have received '+amount +' usd in your account'
    }, function(err, mail) {
      if(err)
      {
        console.log(err);
      }
      else{
      console.log('email sent!');
      }
    });   

});
                }
            else{
                if(currency == "pkr")
                {
                    Transactions.app.models.Account.update({_id:Sid}, {"accountBalance.pkr":  document.accountBalance.pkr - amount},function(err,document){
    console.log('Record updated successfully');
});
   Transactions.create({
          id: "7",
          description:"Transfer From",
          accountId:Sid,
          transectionType:"Debit",
          transectionFrom:document.ownerId,
          referenceId:"23",
          amount:amount,
          currency:currency,
          transactionDate:Date.now()
        },function(err, resp){
        }); 
                Transactions.app.models.Email.send({
      to: 'zunnurainbadar@gmail.com',
      from: 'zunnurain@hybridlogics.com',
      subject: 'Welcome to hybridlogics',
      text: 'Thanx for creating an account',
      html: 'Thanx for you time !Your account have been debited.You have send '+amount +' PKR from your account'
    }, function(err, mail) {
      if(err)
      {
        console.log(err);
      }
      else{
      console.log('email sent!');
      }
    }); 
        Transactions.app.models.Account.findOne({where:{_id:Rid}},function(err,document){
        Transactions.app.models.Account.update({_id:Rid}, {"accountBalance.pkr":  document.accountBalance.pkr + amount},function(err,document){
    console.log('Record updated successfully');
    cb(null,document);
});
Transactions.create({
          id: "8",
          description:"Transfer To",
          accountId:Rid,
          transectionType:"Credit",
          transectionFrom:document.ownerId,
          referenceId:"23",
          amount:amount,
          currency:currency,
          transactionDate:Date.now()
        },function(err, resp){
        });  
                Transactions.app.models.Email.send({
      to: 'zunnurainbadar@gmail.com',
      from: 'zunnurain@hybridlogics.com',
      subject: 'Welcome to hybridlogics',
      text: 'Thanx for creating an account',
      html: 'Thanx for you time !Your account has been credited.You have received '+amount +' PKR'
    }, function(err, mail) {
      if(err)
      {
        console.log(err);
      }
      else{
      console.log('email sent!');
      }
    }); 
});
         
            }
        }
            }
        
    });
}
Transactions.remoteMethod('transfer',{
    accepts:[{arg:'Sid',type:'string'},
    {arg:'Rid',type:'string'},
    {arg:'amount',type:'number'},
    {arg:'currency',type:'string'}
    ],
    returns:[{arg:'Sid', type:'document'},
    {arg:'Rid',type:'string'},
    {arg:'amount',type:'document'},
    {arg:'currency',type:'string'}
    ]
});
};
