'use strict';

module.exports = function(Account) {
Account.getInfo = function(id,cb){
    // logging.log(time, id, date, user,befor) 
    console.log('This is coming Id'+ id);
    Account.findOne({where:{_id:id}},function(err,document){
        cb(null,document);
    });
};
Account.remoteMethod('getInfo', {
    accepts:{arg:'id',type:'string'},
    returns:{arg:'document',type:'document'}
});
 // remote method before hook
  Account.beforeRemote('getInfo', function(context, unused, next) {
    console.log('Remote methods is about to start');
    console.log('This is context req '+ context.req);
    next();
  });
  // remote method after hook
  Account.afterRemote('getInfo', function(context, remoteMethodOutput, next) {
    console.log('Remote method is ending');
    console.log('This is context result' + context.result);
    next();
  });
  Account.afterRemoteError('getInfo', function(context, next){
    console.log('There is an error');
  });
  Account.addInfo = function(id,currency,ownerId,totalAmount,cb){
 console.log('This is id ' + id);
 console.log('This is currency ' + currency);
 console.log('This is owner id ' + ownerId);
 console.log('This is totalAmount ' + totalAmount);
    Account.create({
          id: id,
          currency: currency,
          ownerId: ownerId,
          totalAmount: totalAmount,
          creationDate: "2017-05-25T13:03:25.093Z"
        },function(err, resp){
          cb(err, resp)
        });
  }
  Account.remoteMethod('addInfo',{
    accepts:[{arg:'id',type:'string'},
    {arg:'currency',type:'string'},
    {arg:'ownerId',type:'string'},
    {arg:'totalAmount',type:'number'}
    ],
    returns:[{arg:'id',type:'string'},
    {arg:'currency',type:'string'},
    {arg:'ownerId',type:'string'},
    {arg:'totalAmount',type:'number'}
    ]
  });
}

//   {
//   "id": "string",
//   "currency": "string",
//   "ownerId": "string",
//   "totalAmount": 0,
//   "creationDate": "2017-05-25T13:03:25.093Z"
// }